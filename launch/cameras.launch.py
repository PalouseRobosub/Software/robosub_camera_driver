import os
from launch import LaunchDescription
from launch.actions import ExecuteProcess
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(package='robosub_camera_driver',
             executable='camera_pub',
             name='camera_pub',
             parameters=[{'left_serial': 14406634}])
    ])
