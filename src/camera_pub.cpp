/*
 * Program to get images off the sub cameras and publish to ros topics.
 * See spinnaker examples for more info.
 */

#include <rclcpp/rclcpp.hpp>
#include <Spinnaker.h>
#include <sensor_msgs/msg/image.hpp>
#include <SpinGenApi/SpinnakerGenApi.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include "camera_pub.h"

using namespace std::chrono_literals;

CameraPub::CameraPub() : Node("camera_pub") {
    this->cam_system = Spinnaker::System::GetInstance();
    this->left_pub = this->create_publisher<sensor_msgs::msg::Image>(
                                                          "vision/left/raw", 1);
}

CameraPub::~CameraPub() {
    // != cannot be used with CameraPtr because it will throw an exception
    if (!(this->left_cam == nullptr)) {
        this->left_cam->EndAcquisition();
        this->left_cam->DeInit();
        this->left_cam = nullptr;
    }

    this->cam_system->ReleaseInstance();
}

int CameraPub::publish_images() {
    int pub_count = 0;

    // != cannot be used with CameraPtr because it will throw an exception
    if (!(this->left_cam == nullptr)) {
        pub_count++;
	    Spinnaker::ImagePtr image = this->left_cam->GetNextImage(500);
        auto left_msg_ptr = this->img_to_msg(image);
        this->left_pub->publish(*left_msg_ptr);
    }

    return pub_count;
}

void CameraPub::declare_camera_parameters() {
    this->declare_parameter("left_serial");
}

int CameraPub::load_camera_parameters() {
    int load_count = 0;

    if(!this->get_parameter("left_serial", this->left_serial)) {
        RCLCPP_ERROR(this->get_logger(),
                     "failed to load left camera serial number");
    }
    else {
        load_count++;
    }

    return load_count;
}

int CameraPub::load_cameras() {
    int load_count = 0;

    Spinnaker::CameraList cam_list = this->cam_system->GetCameras();

    this->left_cam = cam_list.GetBySerial(std::to_string(this->left_serial));
    if (this->left_cam == nullptr) {
        RCLCPP_WARN(this->get_logger(), "failed to load left camera");
    }
    else if (!this->initialize_camera(this->left_cam)) {
        RCLCPP_WARN(this->get_logger(), "failed to initialize left camera");
    }
    else {
        load_count++;
    }

    cam_list.Clear();
    return load_count;
}

bool CameraPub::initialize_camera(Spinnaker::CameraPtr cam) {
    cam->Init();

    Spinnaker::GenApi::INodeMap & nodeMap = cam->GetNodeMap();

    Spinnaker::GenApi::CEnumerationPtr modePtr =
                                             nodeMap.GetNode("AcquisitionMode");
    if (!Spinnaker::GenApi::IsAvailable(modePtr) ||
        !Spinnaker::GenApi::IsWritable(modePtr)) {
        RCLCPP_WARN(this->get_logger(),
                    "unable to set camera acquisition mode");
        return false;
    }

    Spinnaker::GenApi::CEnumEntryPtr continuousModePtr =
                                          modePtr->GetEntryByName("Continuous");
    if (!Spinnaker::GenApi::IsAvailable(continuousModePtr) ||
        !Spinnaker::GenApi::IsReadable(continuousModePtr)) {
        RCLCPP_WARN(this->get_logger(), "unable to get continuous mode");
        return false;
    }

    long continuousMode = continuousModePtr->GetValue();

    modePtr->SetIntValue(continuousMode);

    cam->BeginAcquisition();

    return true;
}

sensor_msgs::msg::Image::SharedPtr CameraPub::img_to_msg(
                                                     Spinnaker::ImagePtr image){
    Spinnaker::ImagePtr bgr8Image = 
       image->Convert(Spinnaker::PixelFormat_BGR8, Spinnaker::NEAREST_NEIGHBOR);

    cv::Mat cvImage(bgr8Image->GetHeight() + bgr8Image->GetYPadding(),
                    bgr8Image->GetWidth() + bgr8Image->GetXPadding(),
                    CV_8UC3, bgr8Image->GetData(), bgr8Image->GetStride());

    return cv_bridge::CvImage
                        (std_msgs::msg::Header(), "bgr8", cvImage).toImageMsg();
}

int main(int argc, char **argv) {
    rclcpp::init(argc, argv);

    auto cam_pub = std::make_shared<CameraPub>();

    cam_pub->declare_camera_parameters();

    if (cam_pub->load_camera_parameters() < 1) {
        RCLCPP_FATAL(cam_pub->get_logger(), "no camera parameters loaded");
        return -1;
    }

    if (cam_pub->load_cameras() < 1) {
        RCLCPP_FATAL(cam_pub->get_logger(), "no cameras loaded");
        return -1;
    }

    while(rclcpp::ok()){
        cam_pub->publish_images();
        rclcpp::spin_some(cam_pub);
        rclcpp::sleep_for(10ms); // throttle loop
    }
    
    return 0;
}
