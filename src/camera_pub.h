#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <Spinnaker.h>

class CameraPub : public rclcpp::Node {
    public:
        CameraPub();
        ~CameraPub();

        int publish_images();

        void declare_camera_parameters();
        int load_camera_parameters();

        int load_cameras();

    private:
        bool initialize_camera(Spinnaker::CameraPtr cam);

        int left_serial;

        Spinnaker::SystemPtr cam_system;
        Spinnaker::CameraPtr left_cam;

        rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr left_pub;

    	sensor_msgs::msg::Image::SharedPtr img_to_msg(
                                                     Spinnaker::ImagePtr image);
};
